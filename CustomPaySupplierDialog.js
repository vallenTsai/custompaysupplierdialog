sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/Input",
	"sap/m/Button",
	'sap/ui/model/Filter',
	'sap/m/MessageToast'
], function(Control, Input, Button, Filter, MessageToast) {
	"use strict";
	return Control.extend("newtype.sap.generic.CustomPaySupplierDialog", {
		metadata: {
			properties: {
				value: {
					type: "string",
					defaultValue: ""
				}
			},
			aggregations: {
				_input: {
					type: "sap.m.Input",
					multiple: false,
					visibility: "hidden"
				}
			}
		},
		init: function() {
			this.setAggregation("_input", new Input({
				value: this.getValue(), 
				showValueHelp:true,
				valueHelpOnly:true,
				valueHelpRequest:this._onPress.bind(this)
			}));
		},
		setValue: function(sValue) {
			this.setProperty("value", sValue, true);
		},
		_onPress: function(oEvent) {
			var ctrl = this;

			$.ajax("/ErpApi/api/purma")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);

					if (!ctrl._oDialog) {
						ctrl._oDialog = sap.ui.xmlfragment("newtype.sap.generic.view.CustomPaySupplierDialog", ctrl);
					}
					ctrl._oDialog.setModel(oData, "contorlPaySupplier");

					// clear the old search filter
					ctrl._oDialog.getBinding("items").filter([]);

					ctrl._oDialog.open();
				});
		},
		handleClose: function(oEvent) {
			var ctrl = this;
			var aContexts = oEvent.getParameter("selectedContexts");

			if (aContexts && aContexts.length) {
				var Ma003 = aContexts.map(function(oContext) {
					return oContext.getObject().Ma003;
				}).join(", ");
				MessageToast.show("你選擇了:" + Ma003);

				ctrl.setValue(Ma003);
				ctrl.getAggregation("_input").setValue(Ma003);
			} else {
				MessageToast.show("沒資料被選擇");
			}
		},
		handleSearch: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oBinding = oEvent.getSource().getBinding("items");
			var oFilterOR = new Filter([
				new Filter("Ma001", sap.ui.model.FilterOperator.Contains, sValue, false),
				new Filter("Ma003", sap.ui.model.FilterOperator.Contains, sValue, false)
			], false);

			oBinding.filter(oFilterOR);
		},
		renderer: function(oRM, oControl) {
			oRM.write("<div");
			oRM.writeControlData(oControl);
			oRM.addClass("genericPaySupplierDialog");
			oRM.writeClasses();
			oRM.write(">");
			oRM.renderControl(oControl.getAggregation("_input"));
			oRM.renderControl(oControl.getAggregation("_button"));
			oRM.write("</div>");
		}
	});
});